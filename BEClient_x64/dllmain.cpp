// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"
#include <winsock.h>
#include <intrin.h>
#pragma comment(lib,"ws2_32.lib")

///来自互联网的资料，感谢提供资料的好心人
#ifndef WIN32
#define __cdecl
#endif

struct BECL_GAME_DATA
{
	// Game version string (including game name), e.g. "DayZ 0.54.111111"
	const char *pstrGameVersion;

	// IP address (in network byte order) of the server the game is connected to
	unsigned int uiAddress;

	// Port (in network byte order) of the server the game is connected to
	unsigned short usPort;

	// Print message on the screen prefixed with "BattlEye Client:"
	void(__cdecl *pfnPrintMessage)(const char *pstrMessage);

	// Game needs to be restarted via BE Launcher to properly start or update the BE Service; reason = 0 if BE Service is not running properly, reason = 1 if BE Service needs to be updated
	void(__cdecl *pfnRequestRestart)(int iReason);

	// Send packet to BE Server over game's network channel
	void(__cdecl *pfnSendPacket)(const void *pvPacket, size_t nLength);
};

struct BECL_BE_DATA
{
	// Clean up BE Client before unloading when disconnecting from server (this is mandatory); returns "true" if successful
	bool(__cdecl *pfnExit)(void);

	// BE Client's main processing function, call once per engine frame
	void(__cdecl *pfnRun)(void);

	// Send command to BE Client, e.g. by entering "beclient [command]" in the console; optional
	void(__cdecl *pfnCommand)(const char *pstrCommand);

	// Report packet received from BE Server over game's network channel to BE Client
	void(__cdecl *pfnOnReceivePacket)(const void *pvPacket, size_t nLength);
};
BECL_GAME_DATA g_gamedata = {};
bool __cdecl Exit(void)
{
	_tprintf(_T("Game Call Exit %p\r\n"), _ReturnAddress());
	return true;
}
void __cdecl Run(void)
{
	_tprintf(_T("Game Call Run %p\r\n"), _ReturnAddress());
}
void __cdecl Command(const char *pstrCommand)
{
	_tprintf(_T("Game Call Command %p\r\n"), _ReturnAddress());
	printf("Command = %s\r\n", pstrCommand);
}
void __cdecl OnReceivePacket(const void *pvPacket, size_t nLength)
{
	_tprintf(_T("Game Call OnReceivePacket %p\r\n"), _ReturnAddress());
}
int __cdecl GetVer()
{
	//BE的版本号，现在都是第二版
	//MessageBoxW(nullptr, L"What", L"What", MB_OK);
	return 2;
}
bool __cdecl Init(int iIntegrationVersion, const BECL_GAME_DATA *pGameData, BECL_BE_DATA *pBEData)
{
	pBEData->pfnCommand = Command;
	pBEData->pfnExit = Exit;
	pBEData->pfnRun = Run;
	pBEData->pfnOnReceivePacket = OnReceivePacket;
	g_gamedata = *pGameData;

	_tprintf(_T("GameBase = %p\r\n"), GetModuleHandle(nullptr));
	_tprintf(_T("iIntegrationVersion = %d\r\n"), iIntegrationVersion);
	printf("pstrGameVersion = %s\r\n", pGameData->pstrGameVersion);
	
	in_addr addr = {};
	addr.S_un.S_addr = pGameData->uiAddress;
	auto pIp = inet_ntoa(addr);
	printf("IP = %s\r\n", pIp);
	_tprintf(_T("Port = %d\r\n"), htons(pGameData->usPort));

	_tprintf(_T("pfnPrintMessage = %p\r\n"), pGameData->pfnPrintMessage);

	_tprintf(_T("pfnRequestRestart = %p\r\n"), pGameData->pfnRequestRestart);

	_tprintf(_T("pfnSendPacket = %p\r\n"), pGameData->pfnSendPacket);
	return true;
}

void alloc_cmd_window()
{
	AllocConsole();
	//SetConsoleTitle(_T("输出"));
	AttachConsole(GetCurrentProcessId());

	FILE* pFile = nullptr;
	freopen_s(&pFile, "CON", "r", stdin);
	freopen_s(&pFile, "CON", "w", stdout);
	freopen_s(&pFile, "CON", "w", stderr);
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		alloc_cmd_window();
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

